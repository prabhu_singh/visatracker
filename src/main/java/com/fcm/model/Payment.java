package com.fcm.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.annotation.CreatedDate;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "Payment_from")
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class Payment {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
    private String employeeName;
	private String branch;
	private String displayCartNumber;
	private String displayCartNumberRadio;
	private String beneficiaryName;
    private int amount;
	private String email;
	private String email1;
	private String email2;
	private String invoiceNumber;
    private String beanBankAccountNumber;
	private String ifscBankCode;
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedAt;

    @Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_at", updatable = false)
	private Date createdAt;

	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getEmployeeName() {
		return employeeName;
	}
	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}
	public String getBranch() {
		return branch;
	}
	public void setBranch(String branch) {
		this.branch = branch;
	}
	public String getDisplayCartNumber() {
		return displayCartNumber;
	}
	public void setDisplayCartNumber(String displayCartNumber) {
		this.displayCartNumber = displayCartNumber;
	}
	public String getDisplayCartNumberRadio() {
		return displayCartNumberRadio;
	}
	public void setDisplayCartNumberRadio(String displayCartNumberRadio) {
		this.displayCartNumberRadio = displayCartNumberRadio;
	}
	public String getBeneficiaryName() {
		return beneficiaryName;
	}
	public void setBeneficiaryName(String beneficiaryName) {
		this.beneficiaryName = beneficiaryName;
	}
	public int getAmount() {
		return amount;
	}
	public void setAmount(int amount) {
		this.amount = amount;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getEmail1() {
		return email1;
	}
	public void setEmail1(String email1) {
		this.email1 = email1;
	}
	public String getEmail2() {
		return email2;
	}
	public void setEmail2(String email2) {
		this.email2 = email2;
	}
	public String getInvoiceNumber() {
		return invoiceNumber;
	}
	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}
	public String getBeanBankAccountNumber() {
		return beanBankAccountNumber;
	}
	public void setBeanBankAccountNumber(String beanBankAccountNumber) {
		this.beanBankAccountNumber = beanBankAccountNumber;
	}
	public String getIfscBankCode() {
		return ifscBankCode;
	}
	public void setIfscBankCode(String ifscBankCode) {
		this.ifscBankCode = ifscBankCode;
	}
	public Date getUpdatedAt() {
		return updatedAt;
	}
	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}
	public Date getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}
    
}