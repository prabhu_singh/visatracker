/*
 * package com.fcm.filter;
 * 
 * import java.io.IOException; import java.util.Collections; import
 * java.util.Enumeration; import java.util.Map; import java.util.TreeMap;
 * 
 * import javax.servlet.Filter; import javax.servlet.FilterChain; import
 * javax.servlet.ServletException; import javax.servlet.ServletRequest; import
 * javax.servlet.ServletResponse; import javax.servlet.http.HttpServletRequest;
 * import javax.servlet.http.HttpServletRequestWrapper; import
 * javax.servlet.http.HttpServletResponse;
 * 
 * import org.springframework.beans.factory.annotation.Autowired; import
 * org.springframework.core.annotation.Order; import
 * org.springframework.stereotype.Component;
 * 
 * import com.fcm.common.MasterUtil; import com.fcm.common.WebUtils;
 * 
 * @Component
 * 
 * @Order(3) public class SimpleFilter implements Filter{
 * 
 * @Autowired private WebUtils webUtils;
 * 
 * public void doFilter(ServletRequest req, ServletResponse response,
 * FilterChain chain) throws IOException, ServletException { Map<String,
 * String[]> extraParams = new TreeMap<String, String[]>(); HttpServletRequest
 * httpservletReq = (HttpServletRequest) req; String url = ((HttpServletRequest)
 * req).getRequestURL().toString(); if (url.contains("/oauth/token")) {
 * webUtils.setRequest(httpservletReq); try { HttpServletRequest wrappedRequest
 * = new AuthLoginModifyRequest(httpservletReq, extraParams);
 * chain.doFilter(wrappedRequest, response); } catch (Exception e) { } } else {
 * chain.doFilter(req, response); } }
 * 
 * private static class AuthLoginModifyRequest extends HttpServletRequestWrapper
 * { private final Map<String, String[]> modifiableParameters; private
 * Map<String, String[]> allParameters = null;
 * 
 * 
 * 
 * public AuthLoginModifyRequest(final HttpServletRequest request, final
 * Map<String, String[]> additionalParams) { super(request);
 * modifiableParameters = new TreeMap<String, String[]>();
 * modifiableParameters.putAll(additionalParams); }
 * 
 * 
 * 
 * @Override public Enumeration<String> getParameterNames() { return
 * super.getParameterNames(); }
 * 
 * 
 * 
 * @Override public String[] getParameterValues(final String name) { if (name !=
 * null && name.equals("password")) { String password =
 * getPassword(super.getParameterValues(name)[0]); return new String[] {
 * password }; } else { return super.getParameterValues(name); } }
 * 
 * 
 * 
 * private String getPassword(String password){ try{
 * password=MasterUtil.decrytBase64(password);
 * password=MasterUtil.decryptAES(password); } catch(Exception e){ } return
 * password; }
 * 
 * 
 * 
 * @Override public Map<String, String[]> getParameterMap() { if (allParameters
 * == null) { allParameters = new TreeMap<String, String[]>();
 * allParameters.putAll(super.getParameterMap()); String password =
 * super.getParameter("password"); password = getPassword(password);
 * allParameters.put("password", new String[] { password });
 * allParameters.putAll(modifiableParameters); } return
 * Collections.unmodifiableMap(allParameters); } } }
 */