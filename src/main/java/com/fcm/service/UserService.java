package com.fcm.service;

import com.fcm.model.User;
import com.fcm.request.PasswordRequest;

public interface UserService {

	User findByEmail(String email);

	void changePassword(String email, PasswordRequest request);

	void update(String token, String ipAddress, long id);
	
	
	

}
