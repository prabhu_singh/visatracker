package com.fcm.service;

import java.util.Date;
import java.util.List;

import org.springframework.web.bind.annotation.RequestParam;

import com.fcm.model.Payment;
import com.fcm.request.PaymentRequest;

public interface PaymentService {

	Payment savePayment(Payment payment);
	
	List<Payment> fetchAllPayment();

	List<Payment> findByCreatedAtBetween(Date fromDate, Date toDate);

}
