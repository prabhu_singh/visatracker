package com.fcm.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.fcm.model.Payment;
import com.fcm.request.PaymentRequest;

@Repository
public interface PaymentRepository extends JpaRepository<Payment, Long> {

	PaymentRequest save(PaymentRequest paymentRequest);

	@Query("SELECT p from Payment p where (( p.createdAt >= :startDate) AND ( p.createdAt <= :endDate))")
	List<Payment> findByCreatedAtBetween(@Param("startDate") Date startDate, @Param("endDate") Date endDate);

}
