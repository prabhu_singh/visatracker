package com.fcm.serviceImpl;

import java.util.NoSuchElementException;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.fcm.model.User;
import com.fcm.repository.UserRepository;
import com.fcm.request.PasswordRequest;
import com.fcm.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Service
public class UserServiceImpl implements UserService {

	private final Logger LOGGER = LoggerFactory.getLogger(UserServiceImpl.class);

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Autowired
	UserRepository usersRepository;

	@Override
	public User findByEmail(String email) {

		return usersRepository.findByEmail(email);
	}

	@Override
	public void changePassword(String email, PasswordRequest request) {
		try {
			Optional<User> user = Optional.ofNullable(usersRepository.findByEmail(email));

			if (!user.isPresent())
				throw new NoSuchElementException("Email Id does not exist in system !");

			LOGGER.info("Password change for " + user.get().getEmail());

			user.get().setPassword(passwordEncoder.encode(request.getNewPassword()));

			usersRepository.save(user.get());

		} catch (Exception e) {
			throw new RuntimeException(
					"ERROR: " + e.getMessage() != null && e.getMessage().length() < 75 ? e.getMessage()
							: "Your request fail to complete!");
		}

	}

	@Override
	public void update(String token, String ipAddress, long id) {
		Optional<User> t = usersRepository.findById(id);
		if (t == null || !t.isPresent()) {
			throw new NoSuchElementException("User data not found");
		}
		t.get().setResetToken(token);
		t.get().setIpAddress(ipAddress);
		usersRepository.save(t.get());
	}

}
