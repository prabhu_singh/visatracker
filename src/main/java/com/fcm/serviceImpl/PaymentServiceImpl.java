package com.fcm.serviceImpl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;

import com.fcm.model.Payment;
import com.fcm.repository.PaymentRepository;
import com.fcm.request.PaymentRequest;
import com.fcm.service.PaymentService;

@Service
public class PaymentServiceImpl implements PaymentService {

	@Autowired
	private PaymentRepository paymentRepository;

	public Payment savePayment(Payment payment) {
		 String Noida = payment.getDisplayCartNumber().trim();
		String account = payment.getBeanBankAccountNumber();
	    String subRefNo=payment.getDisplayCartNumber().substring(0,3).trim();
	    if((Noida.equals("Yes"))&&((subRefNo.equals("DEL"))||(subRefNo.equals("del")))) {
	    	subRefNo="NOI";
	    }else {
	    	subRefNo=subRefNo;
	    }
	    switch (subRefNo) {
	      case "BOM":
	      case "bom":
	    	  account = "019700970001";
	          break;
	     
	      case "CCU":
	      case "ccu":
	    	  account = "025130980001";
	          break;
	          
	      case "MAA":
	      case "maa":	  
	    	  account = "041573916001";
	          break;

	      case "BLR":
	      case "blr":
	    	  account = "072106891001";
	          break;

	      case "HYD":
	      case "hyd":  
	    	  account = "081506537001";
	          break;
	          
	      case "AHM":
	      case "ahm":  
	    	  account = "101192623001";
	          break;

	      case "PNQ":
	      case "pnq":
	    	  account = "105315741001";
	          break;

	      case "IXC":
	      case "ixc":
	    	  account = "125170589001";
	          break;
	   
	      case "DEL":
	      case "del":
	    	  account = "053097713001";
	          break;

	      case "HOO":
	      case "hoo":
	    	  account = "053097713003";
	          break;

	      case "NOI":
	      case "noi":
	    	  account = "054210422916";
	          break;

	      case "FSP":
	      case "fsp":
	    	  account = "053097713002";
	          break;

	      case "CDE":
	      case "cde":
	    	  account = "053097713001";
	          break;

	      case "CMU":
	      case "cmu":
	    	  account = "019700970001";
	          break;

	      case "CNO":
	      case "cno":
	    	  account = "054210422916";
	          break;
	          	
	      case "CKO":
	      case "cko":
	    	  account = "025130980001";
	          break;
	          	
	      case "CHY":
	      case "chy":
	    	  account = "081506537001";
	          break;
	          	
	      case "CBA":
	      case "cba":
	    	  account = "072106891001";
	          break;
	          	
	      case "CPU":
	      case "cpu":
	    	  account = "105315741001";
	          break;
	          	
	      case "CAH":
	      case "cah":
	    	  account = "101192623001";
	          break;
	          	
	      case "CCH":
	      case "cch":
	    	  account = "125170589001";
	          break;
	          	
	      case "CHE":
	      case "che":
	    	  account = "041573916001";
	          break;

	      case "CVA":
	      case "cva":
	    	  account = "101192623001";
	          break;
	          	
	      case "CCO":
	      case "cco":
	    	  account = "041573916001";
	          break;
	          	          
	      case "FDE":
	      case "fde":
	    	  account = "053097713001";
	          break;

	      case "FMU":
	      case "fmu":
	    	  account = "019700970001";
	          break;
	          	
	      case "FNO":
	      case "fno":
	    	  account = "054210422916";
	          break;
	          	
	      case "FKO":
	      case "fko":
	    	  account = "025130980001";
	          break;
	          	
	      case "FHY":
	      case "fhy":
	    	  account = "081506537001";
	          break;
	          	
	      case "FBA":
	      case "fba":
	    	  account = "072106891001";
	          break;
	          	
	      case "FPU":
	      case "fpu":
	    	  account = "105315741001";
	          break;
	          
	      case "FAH":
	      case "fah":
	    	  account = "101192623001";
	          break;
	          	
	      case "FCH":
	      case "fch":
	    	  account = "125170589001";
	          break;
	          	
	      case "FHE":
	      case "fhe":
	    	  account = "041573916001";
	          break;
	          	
	      case "FVA":
	      case "fva":
	    	  account = "101192623001";
	          break;
	          	
	      case "FCO":
	      case "fco":
	    	  account = "041573916001";
	          break;
	          
	      case "CTR":
	      case "ctr":
	    	  account = "051346633903";
	          break;

	      case "FTR":
	      case "ftr":
	    	  account = "051346633903";
	          break;    	  
	     }//switch ends
	    Payment payment1=new Payment();
	    payment1.setCreatedAt(payment.getCreatedAt());
	    payment1.setEmployeeName(payment.getEmployeeName());
	    payment1.setBranch(payment.getBranch());
	    payment1.setDisplayCartNumber(payment.getDisplayCartNumber());
	    payment1.setDisplayCartNumberRadio(payment.getDisplayCartNumberRadio());
	    payment1.setBeneficiaryName(payment.getBeneficiaryName());
	    payment1.setAmount(payment.getAmount());
	    payment1.setEmail(payment.getEmail());
	    payment1.setEmail1(payment.getEmail1());
	    payment1.setEmail2(payment.getEmail2());
	    payment1.setInvoiceNumber(payment.getInvoiceNumber());
	    payment1.setBeanBankAccountNumber(account);;
	    payment1.setIfscBankCode(payment.getIfscBankCode());
		return paymentRepository.save(payment1);
	}

	@Override
	public List<Payment> fetchAllPayment() {

		return paymentRepository.findAll();
	}

	@Override
	public List<Payment> findByCreatedAtBetween(Date fromDate, Date toDate) {

		return paymentRepository.findByCreatedAtBetween(fromDate, toDate);

	}

}
