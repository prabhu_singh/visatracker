package com.fcm.validators;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.fcm.request.PasswordRequest;



@Component("passwordValidator")
public class PasswordValidator implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {
		return PasswordRequest.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		PasswordRequest tr = (PasswordRequest) target;
//		if (tr.getCurrentPassword() == null || tr.getCurrentPassword().equals("")) {
//			errors.rejectValue("password", "", " Password can not empty.");
//		}
		if (tr.getNewPassword() == null || tr.getNewPassword().equals("")) {
			errors.rejectValue("newPassword", "", " New Password can not empty.");

		}
		if (tr.getConfirmPassword() == null || tr.getConfirmPassword().equals("")) {
			errors.rejectValue("confirmPassword", "", " Confirm Password can not empty.");

		}
		if (tr.getNewPassword() != null && tr.getConfirmPassword() != null) {
		  if(!tr.getNewPassword().equals(tr.getConfirmPassword())){
				errors.rejectValue("newPassword", "", " New Password changePassword mismatch.");
		  }
		  if(tr.getNewPassword().length()<6){
				errors.rejectValue("newPassword", "", "Please enter New password more than 6 character");
		  }
		}

	}
}
