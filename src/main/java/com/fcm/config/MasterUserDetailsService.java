package com.fcm.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.fcm.model.User;
import com.fcm.repository.UserRepository;
@Service
public class MasterUserDetailsService implements UserDetailsService{

	@Autowired
	private UserRepository usersRepository;

	@Override
	public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
		User user = this.usersRepository.findByEmail(email);
		if (user == null)
			throw new UsernameNotFoundException("This email doesn't exit!");
		return new PrincipalUserDetails(user);
	}

}
