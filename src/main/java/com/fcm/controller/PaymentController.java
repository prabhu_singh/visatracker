package com.fcm.controller;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fcm.model.Payment;
import com.fcm.request.PaymentRequest;
import com.fcm.service.PaymentService;

@RestController
@RequestMapping("/api")
public class PaymentController {
	@Autowired
	private PaymentService paymentService;

	@PostMapping("/payment")
	public Payment savePayment(@ModelAttribute Payment payment) {
		payment.setCreatedAt(new Date());

		return paymentService.savePayment(payment);
	}

	@GetMapping("/getAllPaymentData")
	public List<Payment> fetchAllPayment() {

		return paymentService.fetchAllPayment();
	}

	@PostMapping("/getDataByDates")
	public List<Payment> getPaymentByCreatedDate(@RequestParam String fromDate, @RequestParam String toDate)
			throws ParseException {

		Date fromDate1 = null;
		try {
			fromDate1 = new SimpleDateFormat("yyyy-MM-dd").parse(fromDate);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		Date toDate1 = null;
		try {
			toDate1 = new SimpleDateFormat("yyyy-MM-dd").parse(toDate);
		} catch (ParseException e) {

			e.printStackTrace();
		}
		return paymentService.findByCreatedAtBetween(fromDate1, toDate1);
	}
}
