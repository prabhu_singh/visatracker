package com.fcm.controller;

import javax.xml.bind.ValidationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fcm.model.User;
import com.fcm.request.PasswordRequest;
import com.fcm.service.UserService;
import com.fcm.validators.PasswordValidator;

@RestController
@RequestMapping("/visa")
public class UserController {

	@Autowired
	PasswordValidator passwordValidator;

	@Autowired
	UserService userService;

	@GetMapping("/user/findByEmail")
	public User findByEmail(@RequestParam(value = "email", required = false) String email) {
		return userService.findByEmail(email);
	}

	@PostMapping("/changePassword/{email}")
	public void changeUserPassword(@PathVariable String email, @ModelAttribute("request") PasswordRequest request,
			BindingResult result) throws Exception {

		passwordValidator.validate(request, result);

		if (result.hasErrors()) {
			throw new ValidationException("ERROR: " + result.getFieldError().getDefaultMessage());
		}

		userService.changePassword(email, request);
	}

}
