package com.fcm.request;


public class PaymentRequest {
	
	private long id;
	private String employeeName;
	private String branch;
	private String displayCartNumber;
	private String displayCartNumberRadio;
	private String beneficiaryName;
	private int amount;
	private String email;
	private String email1;
	private String email2;
	private String invoiceNumber;
	private String beanBankAccountNumber;
	private String ifscBankCode;

	/*
	 * private String fromDate; private String toDate;
	 */
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getEmployeeName() {
		return employeeName;
	}
	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}
	public String getBranch() {
		return branch;
	}
	public void setBranch(String branch) {
		this.branch = branch;
	}
	public String getDisplayCartNumber() {
		return displayCartNumber;
	}
	public void setDisplayCartNumber(String displayCartNumber) {
		this.displayCartNumber = displayCartNumber;
	}
	public String getDisplayCartNumberRadio() {
		return displayCartNumberRadio;
	}
	public void setDisplayCartNumberRadio(String displayCartNumberRadio) {
		this.displayCartNumberRadio = displayCartNumberRadio;
	}
	public String getBeneficiaryName() {
		return beneficiaryName;
	}
	public void setBeneficiaryName(String beneficiaryName) {
		this.beneficiaryName = beneficiaryName;
	}
	public int getAmount() {
		return amount;
	}
	public void setAmount(int amount) {
		this.amount = amount;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getEmail1() {
		return email1;
	}
	public void setEmail1(String email1) {
		this.email1 = email1;
	}
	public String getEmail2() {
		return email2;
	}
	public void setEmail2(String email2) {
		this.email2 = email2;
	}
	public String getInvoiceNumber() {
		return invoiceNumber;
	}
	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}
	public String getBeanBankAccountNumber() {
		return beanBankAccountNumber;
	}
	public void setBeanBankAccountNumber(String beanBankAccountNumber) {
		this.beanBankAccountNumber = beanBankAccountNumber;
	}
	public String getIfscBankCode() {
		return ifscBankCode;
	}
	public void setIfscBankCode(String ifscBankCode) {
		this.ifscBankCode = ifscBankCode;
	}
	
	
    
	
}
