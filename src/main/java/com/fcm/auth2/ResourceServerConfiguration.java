
package com.fcm.auth2;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.provider.error.OAuth2AccessDeniedHandler;

@Configuration
@EnableResourceServer
public class ResourceServerConfiguration extends ResourceServerConfigurerAdapter {

//	@Override
//	public void configure(HttpSecurity httpSecurity) throws Exception {
//		httpSecurity.csrf().disable().authorizeRequests().antMatchers(HttpMethod.OPTIONS, "/oauth/token").permitAll()
//				.antMatchers(HttpMethod.OPTIONS, "/api/**")
//				//.hasAnyRole(AuthoritiesConstant.SUPER,AuthoritiesConstant.RetailCustomer,AuthoritiesConstant.AgentFFMC,AuthoritiesConstant.AgentStudentConsultantTravelAgent,AuthoritiesConstant.AgentADii,AuthoritiesConstant.BusinessTraveller,AuthoritiesConstant.BusinessTravellerAdmin,AuthoritiesConstant.FCMAdmin,AuthoritiesConstant.FCMStoreManager,AuthoritiesConstant.FCMSales,AuthoritiesConstant.FCMFulfilment,AuthoritiesConstant.FCMDispatch,AuthoritiesConstant.FCMApprove,AuthoritiesConstant.FCMApproveCorp).antMatchers("/user/**", "/api/**", "/dashboard/**")
//				//.hasAnyRole(AuthoritiesConstant.SUPER,AuthoritiesConstant.RetailCustomer,AuthoritiesConstant.AgentFFMC,AuthoritiesConstant.AgentStudentConsultantTravelAgent,AuthoritiesConstant.AgentADii,AuthoritiesConstant.BusinessTraveller,AuthoritiesConstant.BusinessTravellerAdmin,AuthoritiesConstant.FCMAdmin,AuthoritiesConstant.FCMStoreManager,AuthoritiesConstant.FCMSales,AuthoritiesConstant.FCMFulfilment,AuthoritiesConstant.FCMDispatch,AuthoritiesConstant.FCMApprove,AuthoritiesConstant.FCMApproveCorp).anyRequest().authenticated().and().formLogin()
//				.loginPage("/login").permitAll().and().logout().logoutSuccessUrl("/").and().sessionManagement()
//				.sessionCreationPolicy(SessionCreationPolicy.STATELESS).and().exceptionHandling()
//				.accessDeniedHandler(new OAuth2AccessDeniedHandler());
//	}
	
	public void configure(HttpSecurity http) throws Exception {
		http.csrf().disable().authorizeRequests().antMatchers(HttpMethod.OPTIONS,"/oauth/token").permitAll()
		.antMatchers(HttpMethod.OPTIONS,"/api/**").authenticated().and().formLogin().loginPage("/login").permitAll().and().logout().logoutSuccessUrl("/").and().sessionManagement()
		.sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
		.exceptionHandling()
		.accessDeniedHandler(new OAuth2AccessDeniedHandler());
		//super.configure(http);
	}
	   
}

