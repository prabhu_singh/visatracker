package com.fcm.auth2;

import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.stereotype.Component;

import com.fcm.model.User;
import com.fcm.service.UserService;

@Component
public class StoreAccessToken {
	
	@Autowired
	UserService userService;
	
	 class CustomTokenEnhancerChain implements TokenEnhancer {
			

			private List<TokenEnhancer> delegates = Collections.emptyList();
			public void setTokenEnhancers(List<TokenEnhancer> delegates) {
				this.delegates = delegates;
			}

			public OAuth2AccessToken enhance(OAuth2AccessToken accessToken, OAuth2Authentication authentication) {
				OAuth2AccessToken result = accessToken;
				String	ipAddress="";
				for (TokenEnhancer enhancer : delegates) {
					result = enhancer.enhance(result, authentication);
				}

				if(result!=null && result.getValue().length()>200){
					Object details = SecurityContextHolder.getContext().getAuthentication().getDetails();
					if (details instanceof WebAuthenticationDetails)
						ipAddress = ((WebAuthenticationDetails) details).getRemoteAddress();
					User user = (User) authentication.getPrincipal();
					int i=result.getValue().lastIndexOf(".")+1;
					String token=result.getValue().substring(i);
					userService.update(token, ipAddress, user.getId());
				}
				return result;
			}
		}

}
